//
//  CatListViewModel.swift
//  CatList
//
//  Created by MacBook on 10/01/18.
//  Copyright © 2018 InalBansal. All rights reserved.
//

import Foundation

struct CatListViewModel{
    var genders: [GenderViewModel] = []
}

struct GenderViewModel {
    var cats: [String]
    var name: String
}
