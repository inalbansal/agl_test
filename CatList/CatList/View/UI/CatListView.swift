//
//  CatListView.swift
//  CatList
//
//  Created by MacBook on 10/01/18.
//  Copyright © 2018 InalBansal. All rights reserved.
//

import UIKit

class CatListView: UIView {
    
    @IBOutlet var catListTableView: UITableView!
    var viewModel = CatListViewModel()

    func reload(viewModel: CatListViewModel){
        self.viewModel = viewModel
        DispatchQueue.main.async {[weak self] in
            self?.catListTableView.reloadData()
        }
        
    }
}

extension CatListView: UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.genders.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
       return viewModel.genders[section].name
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.genders[section].cats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CatListTableViewCell", for: indexPath) as! CatListTableViewCell
        cell.catNameLabel.text = viewModel.genders[indexPath.section].cats[indexPath.row]
        return cell
    }
}
