//
//  CatListTableViewCell.swift
//  CatList
//
//  Created by MacBook on 10/01/18.
//  Copyright © 2018 InalBansal. All rights reserved.
//

import UIKit

class CatListTableViewCell: UITableViewCell {
    
    @IBOutlet var catNameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
