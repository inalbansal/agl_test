//
//  CatListViewController.swift
//  CatList
//
//  Created by MacBook on 10/01/18.
//  Copyright © 2018 InalBansal. All rights reserved.
//

import UIKit

class CatListViewController: UIViewController{
    var catListServiceManger = CatListServiceManager()
    var catListView: CatListView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        catListView = self.view as! CatListView
        catListServiceManger.fetch {[weak self] (people, error) in
            if people != nil {
                let viewModel = self?.mapDataModel(dataModel: people!)
                self?.catListView.reload(viewModel: viewModel!)
            }
        }
    }
    
    func mapDataModel(dataModel:PeopleDataModel) -> CatListViewModel{
        
        //Fetch the list of all genders having cat
        var genders: [GenderViewModel] = []
        for (_,person) in dataModel.persons.enumerated() {
            var cats: [String] = []
            guard let pets = person.pets else{continue}
            for(_,pet) in pets.enumerated(){
                if pet.type == "Cat"{
                    cats.append(pet.name)
                }
            }
            let gender = GenderViewModel.init(cats: cats, name: person.gender)
            genders.append(gender)
        }
        
        // Sort all cats as per gender of their owner
        var distinctGenders:[GenderViewModel] = []
        for (_,gender) in genders.enumerated(){
            
            let value =  distinctGenders.contains(where: { (distinctGender) -> Bool in
                return distinctGender.name == gender.name
            })
            if value{
                for (i,distinctGender) in distinctGenders.enumerated(){
                    if distinctGender.name == gender.name
                    {
                        distinctGenders[i].cats.append(contentsOf: gender.cats)
                    }
                }
                
            }else{
                distinctGenders.append(gender)
            }
        }
        
        //Sort the cats in alphabetical order
        for (index,distinctGender) in distinctGenders.enumerated()
        {
            var sortedcats = distinctGender.cats
            // Sort strings in ascending alphabetical order
            sortedcats.sort(by: { (value1: String, value2: String) -> Bool in
                return value1 < value2 })
            print(sortedcats)
            distinctGenders[index].cats = sortedcats
        }
        return CatListViewModel.init(genders: distinctGenders)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

