//
//  CatListServiceManager.swift
//  CatList
//
//  Created by MacBook on 10/01/18.
//  Copyright © 2018 InalBansal. All rights reserved.
//

import Foundation

class CatListServiceManager
{
    lazy var peopleWebService = PeopleWebServiceManager()
    
    func fetch(completionHandler:@escaping (PeopleDataModel?, Error?)->()){
        peopleWebService.getData { (people, error) in
            
            completionHandler(people, error)
            print(people)
            print(error)
        }
        
    }
}
