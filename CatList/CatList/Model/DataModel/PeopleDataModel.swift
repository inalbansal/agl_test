//
//  PeopleDataModel.swift
//  CatList
//
//  Created by MacBook on 10/01/18.
//  Copyright © 2018 InalBansal. All rights reserved.
//

import Foundation

struct PeopleDataModel: Decodable {
    var persons:[Person]
}
struct Person: Decodable {
    var name: String
    var gender: String
    var age: Int
    var pets: [Pet]?
}

struct Pet : Decodable{
    var type: String
    var name: String
}
