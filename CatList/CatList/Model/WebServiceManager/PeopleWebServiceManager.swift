//
//  PeopleWebServiceManager.swift
//  CatList
//
//  Created by MacBook on 10/01/18.
//  Copyright © 2018 InalBansal. All rights reserved.
//

import Foundation

class PeopleWebServiceManager{

    func getData(completionHandler:@escaping (PeopleDataModel?, Error?)->()){
        
        URLSession.shared.dataTask(with: URL.init(string: "http://agl-developer-test.azurewebsites.net/people.json")!) { (data, response, error) in
            
            if error != nil{
                completionHandler(nil, error)
                return
            }
            if data == nil{
                completionHandler(nil, error)
                return
            }
            var people: PeopleDataModel?
            do{
                let persons = try JSONDecoder.init().decode([Person].self, from: data!)
                people = PeopleDataModel(persons: persons)
                completionHandler(people, nil)
            }
            catch{
                completionHandler(nil, error)
            }

            }.resume()
        
    }
}
